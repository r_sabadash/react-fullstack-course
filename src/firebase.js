import * as firebase from 'firebase';

const config = {
	apiKey: "AIzaSyDhUe0q0NIKYzAVt2Zv4FqO6YJKPigg0Ko",
	authDomain: "nba-r-anr-r.firebaseapp.com",
	databaseURL: "https://nba-r-anr-r.firebaseio.com",
	projectId: "nba-r-anr-r",
	storageBucket: "nba-r-anr-r.appspot.com",
	messagingSenderId: "541019709139"
};

firebase.initializeApp(config);

const firebaseDB = firebase.database();
const firebaseArticles = firebaseDB.ref('articles');
const firebaseTeams = firebaseDB.ref('teams');
const firebaseVideos = firebaseDB.ref('videos');

const firebaseLoop = (snapshot) => {
	const data = [];
	snapshot.forEach((childSnapshot) => {
		data.push({
			...childSnapshot.val(),
			id: childSnapshot.key
		});
	});

	return data;
};

export {
	firebase,
	firebaseDB,
	firebaseArticles,
	firebaseTeams,
	firebaseVideos,
	firebaseLoop
}