import React, { Component } from 'react';
import axios from 'axios';
import { URL } from '../../config';
import styles from './VideosList.css';
import Button from '../../components/UI/Button/Button';
import VideosTemplate from './VideosTemplate/VideosTemplate';

import {firebaseVideos, firebaseTeams, firebaseLoop, firebaseArticles} from '../../firebase';


class VideosList extends Component {
	state = {
		videos: [],
		teams: [],
		start: this.props.start,
		end: this.props.start + this.props.amount,
		amount: this.props.amount
	};

	componentDidMount () {
		this.request(this.state.start, this.state.end);
	}

	request = (start, end) => {
		if(this.state.teams.length < 1) {
			firebaseTeams.once('value')
				.then((snapshot) => {
					const  teams = firebaseLoop(snapshot);

					this.setState({
						teams
					});
				});
			// axios.get(`${URL}/teams`)
			// 	.then(response => {
			// 		this.setState({
			// 			teams: response.data
			// 		});
			// 	});
		}

		firebaseVideos.orderByChild('id').startAt(start).endAt(end).once('value')
			.then((snapshot) => {
				const videos = firebaseLoop(snapshot);

				this.setState({
					videos: [...this.state.videos, ...videos],
					start,
					end
				});
			});

		// axios.get(`${URL}/videos?_start=${start}&_end=${end}`)
		// 	.then(response => {
		// 		this.setState({
		// 			videos: [...this.state.videos, ...response.data],
		// 			start,
		// 			end
		// 		});
		// 	});
	};

	loadMore = () => {
		let end = this.state.end + this.state.amount;
		this.request(this.state.end + 1, end);
	};

	renderVideos = (type) => {
		let template = null;

		switch(type) {
			case 'card':
				template = <VideosTemplate
					data={this.state.videos}
					teams={this.state.teams} />;
				break;
			default:
				template = null;
		}

		return template;
	};

	render () {
		return (
			<div className={styles.videoList_wrapper}>
				{this.props.title ? <h3><strong>NBA</strong> Videos</h3> : null}
				{this.renderVideos(this.props.type)}
				{this.props.loadmore
					? <Button type='loadmore' loadMore={() => this.loadMore()} text='Load More Videos' />
					: <Button type='linkTo' text='More videos' linkTo='/videos'/>}
			</div>
		);
	}
}

export default VideosList;