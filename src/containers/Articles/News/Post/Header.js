import React from 'react';

import TeamInfo from '../../Elements/TeamInfo';
import PostData from '../../Elements/PostData';

const Header = (props) => {
	const { teamData, date, author } = props;
	return(
		<div>
			{props.teamData ? <TeamInfo team={teamData} /> : null }
			<PostData data={{date, author}} />
		</div>
	)
};

export default Header;