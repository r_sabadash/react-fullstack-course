import React, { Component } from 'react';
import axios from 'axios';
import { URL } from '../../../../config';
import styles from '../../Articles.css';
import Header from './Header'
import Body from './Body'

import { firebase, firebaseDB, firebaseLoop, firebaseTeams } from '../../../../firebase';

class NewsArticle extends Component {
	state = {
		article: [],
		team: [],
		imageURL: ''
	};

	componentDidMount () {
		firebaseDB.ref(`articles/${this.props.match.params.id}`).once('value')
			.then((snapshot) => {
				let article = snapshot.val();

				firebaseTeams.orderByChild('teamId').equalTo(article.team).once('value')
					.then((snapshot) => {
						const team = firebaseLoop(snapshot);

						this.setState({
							article,
							team
						});
						this.getImageURL(article.image);
					});
			});
		// axios.get(`${URL}/articles?id=${this.props.match.params.id}`)
		// 	.then(response => {
		// 		let article = response.data[0];
		//
		// 		axios.get(`${URL}/teams?id=${article.team}`)
		// 			.then(response => {
		// 				this.setState({
		// 					article,
		// 					team: response.data
		// 				});
		// 			});
		// 	})
	}

	getImageURL = (filename) => {
		firebase.storage().ref('images').child(filename).getDownloadURL()
			.then(url => {
				this.setState({
					imageURL: url
				});
			});
	};

	render () {
		const article = this.state.article;
		const team = this.state.team;
		const imageURL = this.state.imageURL;

		return (
			<div>
				<Header
					teamData={team[0]}
					date={article.date}
					author={article.author} />
				<Body article={article} imageURL={imageURL} />
			</div>
		);
	}
}

export default NewsArticle;