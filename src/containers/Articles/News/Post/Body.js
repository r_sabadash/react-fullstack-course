import React from 'react';
import styles from '../../Articles.css';

const Body = (props) => {
	return(
		<div className={styles.articleBody}>
			<h1>{props.article.title}</h1>
			<div className={styles.articleImage} style={{backgroundImage: `url(${props.imageURL})`}}> </div>
			<div className={styles.articleText}
				dangerouslySetInnerHTML={{
					__html: props.article.body
				}}>
			</div>
		</div>
	)
};

export default Body;