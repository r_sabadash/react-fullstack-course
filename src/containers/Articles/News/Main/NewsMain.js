import React from 'react';
import styles from '../../Articles.css';
import NewsSlider from "../../../NewsSlider/NewsSlider";
import NewsList from "../../../NewsList/NewsList";

const NewsMain = (props) => {
	return(
		<div>
			<NewsSlider
				type='featured'
				start={0}
				amount={3}
				settings={{
					dots: false
				}} />
			<NewsList
				type='cardMain'
				loadmore={true}
				start={0}
				amount={10} />
		</div>
	)
};

export default NewsMain;