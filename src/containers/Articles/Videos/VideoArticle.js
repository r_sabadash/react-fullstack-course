import React, { Component } from 'react';
import axios from 'axios';
import { URL } from '../../../config';
import styles from '../Articles.css';
import Header from './Header'
import Body from './Body'
import VideosRelated from '../../VideosList/VideosRelated/VideosRelated';
import { firebaseDB, firebaseTeams, firebaseLoop, firebaseVideos } from "../../../firebase";

class VideoArticle extends Component {
	state = {
		article: [],
		team: [],
		teams: [],
		related: []
	};

	componentDidMount () {
		firebaseDB.ref(`videos/${this.props.match.params.id}`).once('value')
			.then((snapshot) => {
				let article = snapshot.val();

				firebaseTeams.orderByChild('teamId').equalTo(article.team).once('value')
					.then((snapshot) => {
						const team = firebaseLoop(snapshot);

						this.setState({
							article,
							team
						});

						this.getRelated();
					})
			});

		// axios.get(`${URL}/videos?id=${this.props.match.params.id}`)
		// 	.then(response => {
		// 		let article = response.data[0];
		//
		// 		axios.get(`${URL}/teams?id=${article.team}`)
		// 			.then(response => {
		// 				this.setState({
		// 					article,
		// 					team: response.data
		// 				});
		// 			});
		//
		// 		this.getRelated();
		// 	})
	}

	getRelated = () => {
		firebaseTeams.once('value')
			.then((snapshot) => {
				const teams = firebaseLoop(snapshot);

				firebaseVideos.orderByChild('team').equalTo(this.state.article.team).limitToFirst(3).once('value')
					.then((snapshot) => {
						const related = firebaseLoop(snapshot);

						this.setState({
							teams,
							related
						})
					});
			});
		// axios.get(`${URL}/teams`)
		// 	.then((response) => {
		// 		let teams = response.data;
		//
		// 		axios.get(`${URL}/videos?q=${this.state.team[0].city}&_limit=3`)
		// 			.then((response) => {
		// 				this.setState({
		// 					teams,
		// 					related: response.data
		// 				});
		// 			});
		// 	});
	};

	render () {
		const article = this.state.article;
		const team = this.state.team;

		return (
			<div>
				<Header teamData={team[0]} />
				<Body article={article} />
				<VideosRelated
					data={this.state.related}
					teams={this.state.teams} />
			</div>
		);
	}
}

export default VideoArticle;