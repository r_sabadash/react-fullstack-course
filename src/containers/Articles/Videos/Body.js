import React from 'react';
import styles from '../Articles.css';

const Body = (props) => {
	return(
		<div className={styles.videoWrapper}>
			<h1>{props.article.title}</h1>
			<iframe
				title='videoplayer'
				width='100%'
				height='300px'
				src={`https://www.youtube.com/embed/${props.article.url}`}
				> </iframe>
		</div>
	)
};

export default Body;