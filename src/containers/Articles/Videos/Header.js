import React from 'react';

import TeamInfo from '../Elements/TeamInfo';

const Header = (props) => {
	const { teamData } = props;
	return(
		<div>
			{props.teamData ? <TeamInfo team={teamData} /> : null }
		</div>
	)
};

export default Header;