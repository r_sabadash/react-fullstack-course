import React from 'react';
import styles from '../../Articles.css';
import VideosList from "../../../VideosList/VideosList";
// import NewsSlider from "../../../NewsSlider/NewsSlider";
// import NewsList from "../../../NewsList/NewsList";


const VideosMain = (props) => {
	return(
		<div>
			<VideosList
				type='card'
				loadmore={true}
				start={0}
				amount={10}
				title={false} />
		</div>
	)
};

export default VideosMain;