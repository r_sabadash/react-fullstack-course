import React, { Component } from 'react';
import styles from './Dashboard.css';
import FormField from '../../components/UI/FormField/FormField';

import { firebaseTeams, firebaseArticles, firebase } from "../../firebase";

import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertFromRaw, convertToRaw } from 'draft-js';
import { stateToHTML } from 'draft-js-export-html';

import Uploader from '../../components/UI/FileUploader/FileUploader';

class Dashboard extends Component{
	state = {
		editorState: EditorState.createEmpty(),
		postError: '',
		loading: false,
		formData: {
			title: {
				element: 'input',
				value: '',
				config: {
					name: 'title-input',
					type: 'text',
					placeholder: 'Enter the title'
				},
				validation: {
					required: true
				},
				valid: false,
				touched: false,
				validationMessage: ''
			},
			author: {
				element: 'input',
				value: '',
				config: {
					name: 'author-input',
					type: 'text',
					placeholder: 'Enter author name'
				},
				validation: {
					required: true
				},
				valid: false,
				touched: false,
				validationMessage: ''
			},
			body: {
				element: 'texteditor',
				value: '',
				valid: true
			},
			image: {
				element: 'image',
				value: '',
				valid: true
			},
			team: {
				element: 'select',
				value: '',
				config: {
					name: 'team-input',
					options: []
				},
				validation: {
					required: true
				},
				valid: false,
				touched: false,
				validationMessage: ''
			}
		}
	};

	componentDidMount () {
		firebaseTeams.once('value')
			.then((snapshot) => {
				let teams = [];

				snapshot.forEach((childSnapshot) => {
					teams.push({
						id: childSnapshot.val().teamId,
						name: childSnapshot.val().city
					})
				});

				const newFormData = {...this.state.formData};
				const newElement = {...newFormData['team']};

				newElement.config.options = teams;
				newFormData['team'] = newElement;

				this.setState({
					formData: newFormData
				});
			});
	};

	submitForm = (e) => {
		e.preventDefault();

		let data = {};
		let formIsValid = true;

		for(let key in this.state.formData) {
			data[key] = this.state.formData[key].value;
		}

		for(let key in this.state.formData) {
			formIsValid = this.state.formData[key].valid && formIsValid;
		}

		if(formIsValid) {
			this.setState({ loading: true, postError: '' });
			firebaseArticles.orderByChild('id')
				.limitToLast(1).once('value')
				.then(snapshot => {
					let articleId = null;

					snapshot.forEach(childSnapshot => {
						articleId = childSnapshot.val().id
					});

					data['date'] = firebase.database.ServerValue.TIMESTAMP;
					data['id'] = articleId + 1;
					data['team'] = parseInt(data['team']);

					console.log(data);

					firebaseArticles.push(data)
						.then(article => {
							this.props.history.push(`/articles/${article.key}`);
						})
						.catch(e => {
							this.setState({ postError: e.message })
						});
				});
		} else {

		}
	};

	updateForm = (element, content = '') => {
		const newFormData = {...this.state.formData};

		const newElement = {...newFormData[element.id]};

		if(content === '') {
			newElement.value = element.e.target.value;
		} else {
			newElement.value = content;
		}

		if(element.blur) {
			let validData = this.validate(newElement);
			newElement.valid = validData[0];
			newElement.validationMessage = validData[1];
		}

		newElement.touched = element.blur;
		newFormData[element.id] = newElement;

		this.setState({
			formData: newFormData
		});
	};

	validate = (element) => {
		let error = [true, ''];

		if(element.validation.required) {
			const valid = element.value.trim() !== '';
			const message = `${!valid ? 'This field is required' : ''}`;
			error = !valid ? [valid, message] : error
		}

		return error;
	};

	submitButton = () => {
		let buttons = null;

		if (this.state.loading) {
			buttons = 'loading...';
		} else {
			buttons = (
				<div>
					<button type='submit'>Add post</button>
				</div>
			)
		}

		return buttons;
	};

	showError = () => (
		this.state.registerError !== ''
			? <div className={styles.error}>{this.state.registerError}</div>
			: ''
	);

	onEditorStateChange = (editorState) => {
		let contentState = editorState.getCurrentContent();
		let rawState = convertToRaw(contentState);
		let htmlState = stateToHTML(contentState);

		this.updateForm({id: 'body'}, htmlState);

		this.setState({
			editorState
		});
	};

	storeFilename = (filename) => {
		this.updateForm({id: 'image'}, filename);
	};

	render () {
		return (
			<div className={styles.postContainer}>
				<form onSubmit={this.submitForm}>
					<h2>Add post</h2>

					<Uploader onUpload={ (filename) => this.storeFilename(filename)} />

					<FormField
						id={'title'}
						formData={this.state.formData.title}
						change={(element) => this.updateForm(element)} />
					<FormField
						id={'author'}
						formData={this.state.formData.author}
						change={(element) => this.updateForm(element)} />

					<Editor
						editorState={this.state.editorState}
						wrapperClassName='myEditor-wrapper'
						editorClassName='myEditor-editor'
						onEditorStateChange={this.onEditorStateChange} />

					<FormField
						id={'team'}
						formData={this.state.formData.team}
						change={(element) => this.updateForm(element)} />

					{ this.submitButton() }
					{ this.showError() }
				</form>
			</div>
		);
	}
}

export default Dashboard;