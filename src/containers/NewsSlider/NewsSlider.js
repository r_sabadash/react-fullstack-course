import React, { Component } from 'react';
import axios from 'axios';
import SliderTemplates from './SliderTemplates/SliderTemplates'
import { URL } from '../../config';
import { firebaseArticles, firebaseLoop } from '../../firebase';

class NewsSlider extends Component {
	state = {
		news: []
	};

	componentDidMount () {
		firebaseArticles.limitToFirst(this.props.amount).once('value')
			.then((snapshot) => {
				const news = firebaseLoop(snapshot);

				this.setState({
					news
				});
			});
		// axios.get(`${URL}/articles?_start=${this.props.start}&_end=${this.props.amount}`)
		// 	.then(response => {
		// 		this.setState({
		// 			news: response.data
		// 		});
		// 	});
	}

	render () {
		return (
			<div>
				<SliderTemplates
					data={this.state.news}
					type={this.props.type}
					settings={this.props.settings} />
			</div>
		);
	}
}

export default NewsSlider;