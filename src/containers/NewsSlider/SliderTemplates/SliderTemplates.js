import React from 'react';
import Slick from 'react-slick';
import styles from './SliderTemplates.css';
import { Link } from 'react-router-dom';

const SliderTemplates = (props) => {
	let template = null;
	
	const setting = {
		dots: true,
		infinite: true,
		arrows: false,
		speed: 500,
		slidesToShow: 1,
		slidesToScroll: 1,
		...props.settings
	};

	switch(props.type) {
		case 'featured':
			template = props.data.map((item, index) => {
				return (
					<div key={item.id}>
						<div className={styles.featured_item}>
							<div
								className={styles.featured_image}
								style={{backgroundImage: `url(../images/articles/${item.image})`}}> </div>
							<Link to={`articles/${item.id}`}>
								<div className={styles.featured_caption}>
									{item.title}
								</div>
							</Link>
						</div>
					</div>
				);
			});
			break;
		default:
			template = null;
	}

	return (
		<Slick {...setting}>
			{template}
		</Slick>
	);
};

export default SliderTemplates;