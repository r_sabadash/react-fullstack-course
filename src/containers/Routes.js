import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Home from '../components/Home/Home';
import Layout from '../hoc/Layout/Layout';
import NewsArticle from './Articles/News/Post';
import VideoArticle from './Articles/Videos/VideoArticle';
import NewsMain from './Articles/News/Main/NewsMain'
import VideosMain from './Articles/Videos/VideosMain/VideosMain'
import SignIn from "./SignIn/SignIn";
import Dashboard from "./Dashboard/Dashboard";

const Routes = (props) => {

	console.log(props);
	return (
		<Layout user={props.user}>
			<Switch>
				<Route path='/' exact component={Home} />
				<Route path='/news' exact component={NewsMain} />
				<Route path="/articles/:id" exact component={NewsArticle}/>
				<Route path="/videos/:id" exact component={VideoArticle}/>
				<Route path='/videos' exact component={VideosMain} />
				<Route path='/sign-in' exact component={SignIn} />
				<Route path='/dashboard' exact component={Dashboard} />
			</Switch>
		</Layout>
	);
};

export default Routes;