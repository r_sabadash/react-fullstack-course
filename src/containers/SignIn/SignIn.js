import React, { Component } from 'react';
import styles from './SignIn.css';

import FormField from '../../components/UI/FormField/FormField';
import { firebase } from '../../firebase';

class SignIn extends Component {
	state = {
		registerError: '',
		loading: false,
		formData: {
			email: {
				element: 'input',
				value: '',
				config: {
					name: 'email-input',
					type: 'email',
					placeholder: 'Enter your email'
				},
				validation: {
					required: true,
					email: true
				},
				valid: false,
				touched: false,
				validationMessage: ''
			},
			password: {
				element: 'input',
				value: '',
				config: {
					name: 'password-input',
					type: 'password',
					placeholder: 'Enter your password'
				},
				validation: {
					required: true,
					password: true
				},
				valid: false,
				touched: false,
				validationMessage: ''
			}
		}
	};

	updateForm = (element) => {
		const newFormData = {...this.state.formData};

		const newElement = {...newFormData[element.id]};
		newElement.value = element.e.target.value;

		if(element.blur) {
			let validData = this.validate(newElement);
			newElement.valid = validData[0];
			newElement.validationMessage = validData[1];
		}

		newElement.touched = element.blur;
		newFormData[element.id] = newElement;

		this.setState({
			formData: newFormData
		});
	};

	validate = (element) => {
		let error = [true, ''];

		if(element.validation.required) {
			const valid = element.value.trim() !== '';
			const message = `${!valid ? 'This field is required' : ''}`;
			error = !valid ? [valid, message] : error
		}

		if(element.validation.email) {
			const valid = /\S+@\S+\.\S+/.test(element.value);
			const message = `${!valid ? 'Must be a valid email' : ''}`;
			error = !valid ? [valid, message] : error
		}

		if(element.validation.password) {
			const valid = element.value.length >= 5;
			const message = `${!valid ? 'Must be greater than 5' : ''}`;
			error = !valid ? [valid, message] : error
		}

		return error;
	};

	submitButton = () => {
		let buttons = null;

		if (this.state.loading) {
			buttons = 'loading...';
		} else {
			buttons = (
				<div>
					<button onClick={(e) => this.submitForm(e, false)}>Register now</button>
					<button onClick={(e) => this.submitForm(e, true)}>Log in</button>
				</div>
			)
		}

		return buttons;
	};

	showError = () => (
		this.state.registerError !== ''
			? <div className={styles.error}>{this.state.registerError}</div>
			: ''
	);

	submitForm = (e, type) => {
		e.preventDefault();

		if(type !== null) {
			let data = {};
			let formIsValid = true;

			for(let key in this.state.formData) {
				data[key] = this.state.formData[key].value;
			}

			for(let key in this.state.formData) {
				formIsValid = this.state.formData[key].valid && formIsValid;
			}

			if(formIsValid) {
				this.setState({
					loading: true,
					registerError: ''
				});

				if(type) {
					firebase.auth().signInWithEmailAndPassword(data.email, data.password)
						.then(() => {
							this.props.history.push('/');
						})
						.catch((error) => {
							this.setState({
								loading: false,
								registerError: `${error.message}`
							});
						});
				} else {
					firebase.auth().createUserWithEmailAndPassword(data.email, data.password)
						.then(() => {
							this.props.history.push('/');
						})
						.catch((error) => {
							this.setState({
								loading: false,
								registerError: `${error.message}`
							});
						});
				}
			}
		}
	};

	render () {
		return (
			<div className={styles.logContainer}>
				<form onSubmit={(e) => this.submitForm(e, null)}>
					<h2>Register/Log in</h2>
					<FormField
						id={'email'}
						formData={this.state.formData.email}
						change={(element) => this.updateForm(element)} />
					<FormField
						id={'password'}
						formData={this.state.formData.password}
						change={(element) => this.updateForm(element)} />
				</form>

				{ this.submitButton() }
				{ this.showError() }
			</div>
		);
	}
}

export default SignIn;