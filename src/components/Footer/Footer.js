import React from 'react';
import styles from './Footer.css';
import { Link } from 'react-router-dom';
import * as config from '../../config';

const Footer = () => {
	return (
		<footer className={styles.footer}>
			<Link to='/' className={styles.logo}>
				<img src="/images/nba_logo.png" alt="NBA Logo"/>
			</Link>
			<div className={styles.right}>
				@NBA {config.CURRENT_YEAR} All rights reserved.
			</div>
		</footer>
	);
};

export default Footer;