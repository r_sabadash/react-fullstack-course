import React from 'react';
import styles from './SideNavItems.css';
import { Link, withRouter } from 'react-router-dom';
import FontAwesome from 'react-fontawesome';
import { firebase } from "../../../../firebase";

const SideNavItems = (props) => {
	const items = [
		{
			type: styles.option,
			icon: 'home',
			text: 'Home',
			link: '/',
			login: ''
		},
		{
			type: styles.option,
			icon: 'file-text-o',
			text: 'News',
			link: '/news',
			login: ''
		},
		{
			type: styles.option,
			icon: 'play',
			text: 'Videos',
			link: '/videos',
			login: ''
		},
		{
			type: styles.option,
			icon: 'sign-in',
			text: 'Dashboard',
			link: '/dashboard',
			login: false
		},
		{
			type: styles.option,
			icon: 'sign-in',
			text: 'Sign in',
			link: '/sign-in',
			login: true
		},
		{
			type: styles.option,
			icon: 'sign-out',
			text: 'Sign out',
			link: '/sign-out',
			login: false
		}
	];

	const element = (item, index) => (
		<div key={index} className={item.type}>
			<Link to={item.link}>
				<FontAwesome name={item.icon} />{item.text}
			</Link>
		</div>
	);

	const restricted = (item, index) => {
		let template = null;

		if(props.user === null && item.login) {
			template = element(item, index);
		}

		if(props.user !== null && !item.login) {
			if(item.link === '/sign-out') {
				template = (
					<div key={index} className={item.type} onClick={() => {
						firebase.auth().signOut()
							.then(() => {
								props.history.push('/');
							});
					}}>
						<FontAwesome name={item.icon} />{item.text}
					</div>
				);
			} else {
				template = element(item, index);
			}
		}

		return template;

	};

	const menuItems = items.map((item, index) => {
		return item.login !== ''
			? restricted(item, index)
			: element(item, index);
	});

	return (
		<div>
			{menuItems}
		</div>
	);
};

export default withRouter(SideNavItems);