import React from 'react';
import { Link } from 'react-router-dom';
import FontAwesome from 'react-fontawesome';

import styles from './Header.css';

import SideNavigation from './SideNavigation/SideNavigation';

const Header = (props) => {
	return (
		<header className={styles.header}>
			<SideNavigation {...props} />
			<div className={styles.headerOpt}>
				<FontAwesome
					onClick={props.onOpenNav}
					name='bars'
					style={{color: '#dfdfdf', cursor: 'pointer', padding: '10px'}} />
				<Link to='/' className={styles.logo}>
					<img src="/images/nba_logo.png" alt="NBA Logo"/>
				</Link>
			</div>
		</header>
	);
};

export default Header;