import React from 'react';

import NewsSlider from '../../containers/NewsSlider/NewsSlider';
import NewsList from '../../containers/NewsList/NewsList';
import VideosList from '../../containers/VideosList/VideosList';

const Home = () => {
	return (
		<div>
			<NewsSlider
				type='featured'
				start={0}
				amount={3}
				settings={{
					dots: false
				}} />
			<NewsList
				type='card'
				loadmore={true}
				start={3}
				amount={3} />
			<VideosList
				type='card'
				loadmore={false}
				start={0}
				amount={3}
				title={true} />
		</div>
	);
};

export default Home;