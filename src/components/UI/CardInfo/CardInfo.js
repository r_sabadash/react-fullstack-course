import React from 'react';
import FontAwesome from 'react-fontawesome';
import styles from './CardInfo.css';

const CardInfo = (props) => {
	const teamName = props.teams.find((team) => {
		return props.team === team.teamId;
	});

	const formatDate = (date) => {
		return new Date(date).toDateString();
	};

	return (
		<div className={styles.cardNfo}>
			<span className={styles.teamName}>
				{teamName ? teamName.name : null}
			</span>
			<span className={styles.date}>
				<FontAwesome name='clock-o' />
				{formatDate(props.date)}
			</span>
		</div>
	);
};

export default CardInfo;