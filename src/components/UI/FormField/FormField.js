import React from 'react';
import styles from './FormField.css';

const FormField = ({formData, id, change}) => {
	const showError = () => {
		let errorMessage = null;

		if(formData.validation && !formData.valid) {
			errorMessage = (
				<div className={styles.labelError}>
					{formData.validationMessage}
				</div>
			);
		}

		return errorMessage;
	};


	const renderTemplate = () => {
		let formTemplate = null;
		
		switch (formData.element) {
			case 'input':
				formTemplate = (
					<div>
						<input
						{...formData.config}
						value={formData.value}
						onBlur={(e) => change({e, id, blur: true})}
						onChange={(e) => change({e, id, blur: false})} />
					</div>
				);
				break;
			case 'select':
				formTemplate = (
					<div>
						<select
							value={formData.value}
							name={formData.config.name}
							onBlur={(e) => change({e, id, blur: true})}
							onChange={(e) => change({e, id, blur: false})}>
							{formData.config.options.map((item, index) => (
								<option key={index} value={item.id}>{item.name}</option>
							))};
						</select>
					</div>
				);
				break;
			default:
				formTemplate = null;
		}

		return formTemplate;
	};

	const template = renderTemplate();
	const error = showError();

	return (
		<div>
			{ template }
			{ error }
		</div>
	);
};

export default FormField;